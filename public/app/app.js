'use strict';
var app = angular.module('self-app',[]);

// Service
app.factory('pessoaService',function($http) {
	return {
		lista: function(){
			return $http.get('/api/v1/dentists');
		},
		cadastra: function(data){
			return $http.post('/api/dentists', data);
		},
		edita: function(data){
			var id = data.id;
			delete data.id;
			return $http.put('/api/dentists/'+id, data);
		},
		exclui: function(id){
			return $http.delete('/api/dentists/'+id)
		}
	}
});

// Controller
app.controller('pessoaController', function($scope, pessoaService) {
    console.log('tou no controller');
	$scope.listar = function(){
		pessoaService.lista().then(function(data){
            $scope.pessoas = data;
            console.log(data,'data');
		});
	}

	$scope.editar = function(data){
		$scope.pessoa = data;
		$('#myModal').modal('show');
	}

	$scope.salvar = function(){
		if($scope.pessoa.id){
			pessoaService.edita($scope.pessoa).then(function(res){
				$scope.listar();
				$('#myModal').modal('hide');
			});
		}else{
			pessoaService.cadastra($scope.pessoa).then(function(res){
				$scope.listar();
				$('#myModal').modal('hide');
			});
		}
	}

	$scope.excluir = function(data){
		if(confirm("Tem certeza que deseja excluir?")){
			pessoaService.exclui(data.id).then(function(res){
				$scope.listar();
			});
		}
	}
});
1
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16
17
18
19
20
21
22
23
24
25
26
27
28
29
30
31
32
33
34
35
36
37
38
39
40
41
42
43
44
45
46
47
48
49
50
51
52
53
54
55
56
57
58
'use strict';
var app = angular.module('cdg',[]);
 
// Service
app.factory('pessoaService',function($http) {
	return {
		lista: function(){
			return $http.get('/api/pessoas');
		},
		cadastra: function(data){
			return $http.post('/api/pessoas', data);
		},
		edita: function(data){
			var id = data.id;
			delete data.id;
			return $http.put('/api/pessoa/'+id, data);
		},
		exclui: function(id){
			return $http.delete('/api/pessoa/'+id)
		}
	}
});
 
// Controller
app.controller('pessoaController', function($scope, pessoaService) {
	$scope.listar = function(){
		pessoaService.lista().success(function(data){
			$scope.pessoas = data;
		});
	}
 
	$scope.editar = function(data){
		$scope.pessoa = data;
		$('#myModal').modal('show');
	}
 
	$scope.salvar = function(){
		if($scope.pessoa.id){
			pessoaService.edita($scope.pessoa).success(function(res){
				$scope.listar();
				$('#myModal').modal('hide');
			});
		}else{
			pessoaService.cadastra($scope.pessoa).success(function(res){
				$scope.listar();
				$('#myModal').modal('hide');
			});
		}
	}
 
	$scope.excluir = function(data){
		if(confirm("Tem certeza que deseja excluir?")){
			pessoaService.exclui(data.id).success(function(res){
				$scope.listar();
			});
		}
	}
});