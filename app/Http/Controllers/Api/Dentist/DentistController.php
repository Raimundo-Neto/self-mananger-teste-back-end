<?php

namespace App\Http\Controllers\Api\Dentist;

use App\Http\Controllers\Controller;
use App\Models\Dentist\Dentist;

use App\Http\Requests\Dentist\DentistRequest;

use Exception;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Lang;

class DentistController extends Controller
{

    public function __construct()
    {
    }


    private function getLastId()
    {

        return Dentist::get()->count() + 1;
    }


    private function setId($data)
    {

        $data['_id'] = $this->getLastId();
        $data['id'] = $data['_id'];
        return $data;
    }

    public function index()
    {

        return Dentist::get(['id', 'personal_data.contact.phones', 'personal_data.contact.email', 'personal_data.general.name']);
    }



    public function store(DentistRequest $request)
    {


        Dentist::insert($this->setId((array) $request->all()));

        return  response()->json(['success'  => Lang::get('messages.SUCESSO1')]);
    }



    public function update(DentistRequest $request, $id)
    {



        Dentist::where('_id', (int) $id)->update((array) $request->except(['id', '_id']), ['upsert' => true]);

        return  response()->json(['success' => Lang::get('messages.SUCESSO2')]);
    }


    public function edit($id)
    {

        return  Dentist::where('_id', (int) $id)->get();
    }




    public function destroy($id)
    {

        $dentist = Dentist::find((int) $id );
        $dentist->delete();
       return response()->json(['success' => true]);
    }
}
