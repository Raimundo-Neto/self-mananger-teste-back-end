<?php

namespace App\Http\Requests\Dentist;

use App\Http\Requests\Request as Request;
use Illuminate\Support\Facades\Lang;

class DentistRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function wantsJson()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // dd('tou no request');
        return [
            "personal_data.contact.phones.*.value" => "required",
            "personal_data.contact.email" => "required",

            "personal_data.general.cpf" => "required|string",
            "personal_data.general.gender" => "required|string|max:1",
            "personal_data.general.date_of_birth" => "required|date",
            "personal_data.general.rg" => "required|string",
            "personal_data.general.agency" => "required|string",
            "personal_data.general.marital_status" => "required|string",
            "personal_data.general.nationality" => "required|string",
            "personal_data.general.place_of_birth" => "required|string",
            "personal_data.general.occupation" => "required|string",

            "personal_data.address.postcode" => "required|string",
            "personal_data.address.address" => "required|string",
            "personal_data.address.number" => "required|string",
            "personal_data.address.secondary_address" => "string",
            "personal_data.address.county" => "string|required",
            "personal_data.address.city" => "string|required",
            "personal_data.address.state" => "string|required"


            //
        ];
    }

    public function messages()
    {

        return [

          

        ];
    }
}
