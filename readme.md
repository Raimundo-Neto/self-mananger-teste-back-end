# Back-End-Teste-SelfMananger



Simples Back-end API desenvolvido com:

  - Laravel (5.2)
  - PHP(7.2)
  - Mongodb(3.6)

# Instalação

  - Clone a aplicação:
  >> git clone https://gitlab.com/Raimundo-Neto/self-mananger-teste-back-end.git
  
  - Navegue até a raiz da aplicação e instale as dependencias:
   >> composer install

- Configure seu .env file
- importe o banco  que se encontra no diretorio /doc/db/dentists.json
>> mongoimport --db newdb --collection dentists --file dentists.json;

# Execução

Abra um terminal e execute o mongodb server
>> mongd


Abra um terminal e execute a aplicação
>> php artisan serve