<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DentistTest extends TestCase
{
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testBasicExample()
    {
   
        $response = $this->json('POST', '/dentist/store', [

            "personal_data" =>[
                "contact" => [
                  "phones" => array( "type"=> "mobile", "value"=> "98981556378"),
                  "email" => "guilhermesantoss.f90@gmail.com"
                ],
                "general" => [
                  "cpf"=> "41989537405",
                  "name"=> "Raimundo de paula costa neto",
                  "gender"=> "m",
                  "date_of_birth"=> "1994-11-11",
                  "rg"=> "399478863",
                  "agency"=> "SSP",
                  "marital_status"=> "single",
                  "nationality"=> "Brasileiro",
                  "place_of_birth"=> "São Luis",
                  "occupation"=> "Programador"
                ],
                "address"=> [
                  "postcode"=> "79831290",
                  "address"=> "Rua Cabral",
                  "number"=> "13",
                  "secondary_address"=> "",
                  "county"=> "São Raimundo",
                  "city"=> "São Luís",
                  "state"=> "MG"
                ]
            ]
             


        ]);

        $response->assertTrue(true);




    }
}
